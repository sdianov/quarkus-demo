package com.example.handlers;

import com.example.dto.ErrorDto;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.NoResultException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Slf4j
public class NoResultExceptionMapper implements ExceptionMapper<NoResultException> {
    @Override
    public Response toResponse(NoResultException e) {
        return Response.status(400).entity(new ErrorDto(400, "Not found")).build();
    }
}
