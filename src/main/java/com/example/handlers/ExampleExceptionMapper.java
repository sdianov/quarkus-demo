package com.example.handlers;

import com.example.dto.ErrorDto;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Slf4j
public class ExampleExceptionMapper implements ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception e) {
        log.info("Unhandled exception", e);

        return Response.status(500).entity(new ErrorDto(500, e.getMessage())).build();
    }
}
