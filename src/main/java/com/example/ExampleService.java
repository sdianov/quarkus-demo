package com.example;

import com.example.entity.UserEntity;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Optional;

@ApplicationScoped
public class ExampleService {

    @Inject
    EntityManager em;

    @Inject
    ExampleConfiguration configuration;

    public String hello() {
        return configuration.message;
    }

    @Transactional
    public Optional<UserEntity> getUser(String id) {

        var query = em.createQuery("select e from UserEntity e where e.id = :id", UserEntity.class)
                .setParameter("id", id);

        return Optional.ofNullable(query.getSingleResult());
    }
}
