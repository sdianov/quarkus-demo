package com.example;

import com.example.dto.ErrorDto;
import com.example.entity.UserEntity;
import lombok.RequiredArgsConstructor;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.MessageFormat;

@Path("/hello")
@RequiredArgsConstructor(onConstructor_ = {@Inject})
public class ExampleResource {

    private final ExampleService exampleService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return exampleService.hello();
    }

    static void checkNotNull(Object o, Response.Status httpStatus, String text, Object... textParams) {
        if (o == null) {
            throw new WebApplicationException(
                    Response.status(httpStatus)
                            .entity(new ErrorDto(400, MessageFormat.format(text, textParams)))
                            .build()
            );
        }
    }

    @GET
    @Path("/user")
    @Produces(MediaType.APPLICATION_JSON)
    public UserEntity getUser(@QueryParam("id") String id) {
        checkNotNull(id, Response.Status.BAD_REQUEST, "id is required");
        var user = exampleService.getUser(id).orElse(null);
        checkNotNull(user, Response.Status.NOT_FOUND, "user with id={0} not found", id);
        return user;
    }
}