package com.example;

import io.quarkus.arc.config.ConfigProperties;

import javax.validation.constraints.Size;

@ConfigProperties(prefix = "greeting")
public class ExampleConfiguration {

    @Size(max = 20)
    public String message;

}
