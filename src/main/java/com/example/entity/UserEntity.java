package com.example.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_profiles")
@Getter
@Setter
public class UserEntity {

    @Id
    @Column(name = "id")
    private String id;

    private String email;
}
